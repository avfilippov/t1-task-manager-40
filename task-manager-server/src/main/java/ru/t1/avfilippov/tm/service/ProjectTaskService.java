package ru.t1.avfilippov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.IProjectRepository;
import ru.t1.avfilippov.tm.api.repository.ITaskRepository;
import ru.t1.avfilippov.tm.api.service.IConnectionService;
import ru.t1.avfilippov.tm.api.service.IProjectTaskService;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.avfilippov.tm.exception.field.TaskIdEmptyException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.model.Project;
import ru.t1.avfilippov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected IProjectRepository getProjectRepository() {
        return connectionService.getSqlSession().getMapper(IProjectRepository.class);
    }

    @NotNull
    protected ITaskRepository getTaskRepository() {
        return connectionService.getSqlSession().getMapper(ITaskRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository();
            @NotNull final IProjectRepository projectRepository = getProjectRepository();
            @Nullable final Project project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository();
            @NotNull final IProjectRepository projectRepository = getProjectRepository();
            @Nullable final Project project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final Task task : tasks) taskRepository.removeById(userId, task.getId());
            projectRepository.removeById(userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = getTaskRepository();
            @NotNull final IProjectRepository projectRepository = getProjectRepository();
            @Nullable final Project project = projectRepository.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
