package ru.t1.avfilippov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.service.ISaltProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    static String salt(
            @Nullable final ISaltProvider saltProvider,
            @Nullable final String value
    ) {
        if (saltProvider == null) return null;
        @Nullable final String secret = saltProvider.getPasswordSecret();
        @Nullable final Integer iteration = saltProvider.getPasswordIteration();
        return salt(value,secret,iteration);
    }

    static String salt(
            @Nullable final String value,
            @Nullable final String secret,
            @Nullable final Integer iteration
    ) {
        if (value== null || secret == null || iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i< iteration; i++) {
            result = md5(secret+result+secret);
        }
        return result;
    }

    static String md5(@NotNull final String value) {
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NotNull final byte[] bytes = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
