package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @Nullable
    @SneakyThrows
    List<Task> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    @Nullable
    @SneakyThrows
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @NotNull
    @SneakyThrows
    Task add(@Nullable String userId, @Nullable Task model);

    @NotNull
    @SneakyThrows
    Collection<Task> add(@NotNull Collection<Task> models);

    @NotNull
    @SneakyThrows
    Collection<Task> set(@NotNull Collection<Task> models);

    @SneakyThrows
    void clear(@Nullable String userId);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    Task remove(@NotNull String userId, @Nullable Task model);

    @Nullable
    @SneakyThrows
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    @SneakyThrows
    Task removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    Task removeByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    void removeAll(@Nullable String userId);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
