package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Sort;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @Nullable
    @SneakyThrows
    List<Project> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<Project> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    );

    @Nullable
    @SneakyThrows
    List<Project> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    );

    @Nullable
    @SneakyThrows
    Project add(@Nullable String userId, @Nullable Project model);

    @NotNull
    @SneakyThrows
    Collection<Project> add(@NotNull Collection<Project> models);

    @NotNull
    @SneakyThrows
    Collection<Project> set(@NotNull Collection<Project> models);

    @SneakyThrows
    void clear(@Nullable String userId);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Project remove(@NotNull String userId, @Nullable Project model);

    @Nullable
    @SneakyThrows
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    @SneakyThrows
    Project removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    @SneakyThrows
    Project removeByIndex(@Nullable String userId, @Nullable Integer index);

    @SneakyThrows
    void removeAll(@Nullable String userId);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @NotNull String name, String description);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @NotNull String description);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
