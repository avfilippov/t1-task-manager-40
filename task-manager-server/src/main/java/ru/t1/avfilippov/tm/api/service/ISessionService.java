package ru.t1.avfilippov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    @Nullable
    @SneakyThrows
    Session findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    Session add(@Nullable Session model);

    @NotNull
    @SneakyThrows
    Collection<Session> add(@NotNull Collection<Session> models);

    @SneakyThrows
    void clear(@Nullable String userId);

    @NotNull
    @SneakyThrows
    Session remove(@NotNull String userId, @Nullable Session model);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @Nullable
    List<Session> findAll(@Nullable String userId);

}
